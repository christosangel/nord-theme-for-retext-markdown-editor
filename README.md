# Nord Theme for Retext Markdown Editor

This repository contains two files, that are kept in `~/.config/ReText project/` directory.

You can copy and paste them there, just remember to put the right directory in line 11 of `ReText.conf` first:

```
styleSheet=/home/USERNAME/.config/ReText project/style.css

```



[ https://youtu.be/WKQ2qWuQPK4 ]( https://youtu.be/WKQ2qWuQPK4 )

![1.png](1.png)